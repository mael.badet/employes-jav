package mbadet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Entreprise {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String table = "Employes";
		/*
		 * Personnel test = new Personnel(); test.run();
		 */

		try {

			String url = "jdbc:mysql://localhost:3306/Entreprises";
			String user = "mysql";
			String pass = "mysql";
			Connection connect = DriverManager.getConnection(url, user, pass);
			java.sql.Statement stmt = connect.createStatement();
			ResultSet result = stmt.executeQuery("SELECT * FROM " + table);
			while (result.next()) {
				System.out.println("id : " + result.getInt("id"));
				System.out.println("nom : " + result.getString("nom"));
				System.out.println("Status : " + result.getString("status"));
				System.out.println("coef_salaire : " + result.getInt("coef_salaire"));
				System.out.println("nombres de ventes : " + result.getInt("ventes"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
