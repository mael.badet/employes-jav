-- Adminer 4.8.1 MySQL 10.5.15-MariaDB-0+deb11u1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `Employes`;
CREATE TABLE `Employes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `coef_salaire` double DEFAULT NULL,
  `superieur` varchar(100) DEFAULT NULL,
  `id_subalternes` varchar(100) DEFAULT NULL,
  `ventes` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `Employes` (`id`, `nom`, `status`, `coef_salaire`, `superieur`, `id_subalternes`, `ventes`) VALUES
(6,	'Badet',	'cadre',	11.3,	NULL,	NULL,	150);

-- 2022-09-14 13:50:30
