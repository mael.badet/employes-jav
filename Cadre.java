package mbadet;

import java.util.ArrayList;

public class Cadre extends Employe {
	private String leCadre;

	private ArrayList<String> mesSubalternes = new ArrayList<>();

	public Cadre(String leNom, double leCoeff) {
		super(leNom, leCoeff);

		leCadre = leNom;

	}

	public void addsubalterne(String nom) {
		// TODO Auto-generated method stub
		mesSubalternes.add(nom);

	}

	public void afficheSubalterne() {
		System.out.println();
		System.out.println("les subalterne de " + leCadre + " sont :");
		for (String e : mesSubalternes)
			System.out.println(e);
	}

	public void affiche() {
		System.out.println("Bonjour je m'appelle : " + nom + " je gagne : " + this.getSalaire() + " €");
	}

}
