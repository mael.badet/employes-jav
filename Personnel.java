package mbadet;

public class Personnel {
	public Personnel() {
	}

	public void run() {

		Employe bob = new Employe("bob", 35);
		Employe Sylvain = new Employe("Sylvain", 45);
		Employe Thomas = new Employe("Romain", 20);

		Cadre mael = new Cadre("Mael", 365);
		Commercial blaise = new Commercial("blaise", 365, 150);

		bob.afficheToi();
		Sylvain.afficheToi();
		Thomas.afficheToi();
		mael.afficheToi();
		blaise.afficheToi();

		blaise.getSommeSalaire();

	}
}