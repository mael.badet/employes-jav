package mbadet;

public class Employe {

	protected static int compteur = 1;
	String nom;
	protected int matricule;
	protected double coeffSalaire;
	protected static double conv_collective = 11.30;
	protected static double sommeSalaire = 0;

	public Employe(String leNom, double leCoeff) {
		coeffSalaire = leCoeff;
		nom = leNom;
		matricule = compteur;
		compteur++;
	}

	public void afficheToi() {
		System.out.println("Bonjour je m'appelle : " + nom + " mon numéro de matricule est : " + matricule
				+ " je gagne : " + this.getSalaire() + " €");
	}

	public double getSalaire() {
		sommeSalaire += coeffSalaire * conv_collective;
		return coeffSalaire * conv_collective;
	}

	public void getSommeSalaire() {
		System.out.println("la somme de tous les salaires de l'entreprise est de : " + sommeSalaire + " €.");
	}

}
